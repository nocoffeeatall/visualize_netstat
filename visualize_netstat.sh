#!/bin/bash

#Simple script that generates simple visualization from Netstat output
# Parameters: If you wanna generate image give argument 1 or if you wanna generate image and open it give 2. 
#For example: visualize_netstat.sh 1
#

#GENERATE TIMESTAMP
timestamp=$(date +%d_%m_%Y_%H_%M)

#DEFINE FILES
save_image="$timestamp _netstatviz.png"
raw_data="raw_data_file"
dot_file="$timestamp _netstatviz.dot"

#ARGUMENTS
argument1=$1

#GENERATE .DOT-FILE HEADER AND INFORMATION NODE
echo "digraph netstat {" > "$dot_file"
echo "size=60;" >> "$dot_file"
echo "rankdir=LR;" >> "$dot_file"
echo '"'"$HOSTNAME" '\n' "$timestamp" ' \n\n Source port \n Protocol \n Destination port \n State" [color=white];' >> "$dot_file" 

#RUN COMMAND,PARSE AND PUT TO RAW DATA FILE 
netstat -vatun | sed -e '1,2d' |awk -F "[ :]*" '{print  $4, "\t", $1,"\t",  $5, "\t", $6, "\t", $7, "\t", $8}' > "$raw_data"

#Another option is to use set -f to turn off expansion. Othervise echoing * expands and shows filenames
set -f

#GO RAW DATA THRU LINE BY LINE
while read line; do
#SOURCE IP
var1=$(echo $line |awk {'print  $1'})
#PROTOCOL
var2=$(echo $line |awk {'print  $2'})
#SOURCE PORT
var3=$(echo $line |awk {'print  $3'})
#DESTINATION IP
var4=$(echo $line |awk {'print  $4'})
#DESTINATION PORT
var5=$(echo $line |awk {'print  $5'})
#STATE
var6=$(echo $line |awk {'print  $6'})

#GENERATE NODE FROM DATA
echo "node [shape=record];" >> "$dot_file"

if [ $var2 == "tcp" ]
then
echo '"'"$var1"'"' ' ->' '"'"$var4"'"' '[label=''"'"$var3" '\n' "$var2" '\n' "$var5" '\n' "$var6" '",' 'color=blue''];' >> "$dot_file" 
elif [ $var2 == 'udp' ]
then
echo '"'"$var1"'"' ' ->' '"'"$var4"'"' '[label=''"'"$var3" '\n' "$var2" '\n' "$var5" '\n' "$var6" '",' 'style=dashed, color=grey''];' >> "$dot_file" 
else
echo Nothing else!
fi
done < "$raw_data"

echo "}" >> "$dot_file"

#echo "$var5"
#more "$dot_file"

if [[ $argument1 == 1 ]]
then
dot -Tpng "$dot_file" -o "$save_image"
echo "##"
echo "## Saving generated data to: $dot_file and $save_image."
echo "##"
elif [[ $argument1 == 2 ]]
then 
dot -Tpng "$dot_file" -o "$save_image"
echo "##"
echo "## Saving generated data to: $dot_file and $save_image and opening image."
echo "##"
xdg-open "$save_image"
else
echo "## If you wanna generate image give argument 1 or if you wanna generate image and open it give 2. For example: visualize_netstat.sh 1"
echo "##"
echo "## Saving generated data to: $dot_file"
fi
