# Visualize Netstat-output

This is quite quickly created simple bash script to run "netstat -vatun" and parse the output with awk, generate .dot-file and then throw it to Graphviz to generate .png-file.

You can find a bit more about it from: http://betterthannocoffeeatall.com/blog/post4-

It is definitely work in progress.
